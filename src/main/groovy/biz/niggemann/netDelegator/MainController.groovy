package biz.niggemann.netDelegator

import org.springframework.beans.factory.annotation.Value
import org.springframework.http.HttpStatus
import org.springframework.http.MediaType
import org.springframework.http.ResponseEntity
import org.springframework.scripting.ScriptEvaluator
import org.springframework.scripting.groovy.GroovyScriptEvaluator
import org.springframework.scripting.support.StaticScriptSource
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.RestController
import groovy.json.JsonOutput

@SuppressWarnings("unused")
@RestController
class MainController {

    private @Value('${config.file:netDelegator.cfg}') String configFilename

    @GetMapping(path = ["/", "/config"], produces = MediaType.APPLICATION_JSON_VALUE)
    private ResponseEntity<Object> config(@RequestParam(value = "username", required = false) String username,
                                          @RequestParam(value = "key", required = false) String key) {
        def configMap = readConfigIntoConfigMap(configFilename)
        if (checkUser(configMap, username, key)) {
            def tempMap = configMap.findAll {!it.key.endsWith('.key')} // Dont show keys
            def sc = tempMap.get("internal.showconfig")
            if ((sc != null) && (sc.toLowerCase().take(1) != "t"))
                tempMap.clear()
            return new ResponseEntity<Object>(tempMap, HttpStatus.OK)
        } else {
            return new ResponseEntity<Object>("Not authorized", HttpStatus.FORBIDDEN)
        }
    }

    @GetMapping(path = "/exec", produces = MediaType.TEXT_PLAIN_VALUE)
    private ResponseEntity<Object> exec(@RequestParam(value = "script", required = true) String script,
                                        @RequestParam(value = "params", required = false) String[] params,
                                        @RequestParam(value = "username", required = false) String username,
                                        @RequestParam(value = "key", required = false) String key) {
        def retVals = execGroovy(script, params, username, key)
        def retContent = retVals[0].toString()
        HttpStatus retStatus = retVals[1] as HttpStatus
        return new ResponseEntity<Object>(retContent, retStatus)
    }

    @GetMapping(path = "/exectojson", produces = MediaType.APPLICATION_JSON_VALUE)
    private ResponseEntity<Object> execToJSON(@RequestParam(value = "script", required = true) String script,
                                              @RequestParam(value = "params", required = false) String[] params,
                                              @RequestParam(value = "username", required = false) String username,
                                              @RequestParam(value = "key", required = false) String key) {
        def retVals = execGroovy(script, params, username, key)
        def retContent = retVals[0].toString()
        HttpStatus retStatus = retVals[1] as HttpStatus
        return new ResponseEntity<Object>(retContent, retStatus)
    }

    private def execGroovy(String scriptname, String[] params, String username, String key) {
        def retStatus = HttpStatus.NOT_FOUND
        def retVal = "Groovy-Script " + scriptname + " does not exists or is not allowed"
        def groovyFile = new File(scriptname + ".groovy")
        if (groovyFile.exists()) {
            def configMap = readConfigIntoConfigMap(configFilename)
            if (checkUser(configMap, username, key)) {
                def allowedScripts = "," + configMap.get("internal.allowedScripts").toString().toLowerCase().trim() + ","
                if ((allowedScripts.contains("," + scriptname.toLowerCase() + ",") ||
                        ([",,", ",null,", ",yes,", ",true,"].contains(allowedScripts))) &&
                        !([",-,", ",false,", ",no,"].contains(allowedScripts))) {
                    def paramMap = [:]
                    paramMap.put("params", params)
                    ScriptEvaluator evaluator = new GroovyScriptEvaluator()
                    retVal = evaluator.evaluate(new StaticScriptSource(groovyFile.getText()), paramMap as Map<String, Object>)
                    retStatus = HttpStatus.OK
                }
            } else {
                retVal = "Not authorized"
                retStatus = HttpStatus.FORBIDDEN
            }
        }
        return [retVal, retStatus]
    }

    @GetMapping(path = "/start", produces = MediaType.APPLICATION_JSON_VALUE)
    private ResponseEntity<Object> start(@RequestParam(value = "programID", required = true) String programID,
                                         @RequestParam(value = "params", required = false) String[] params,
                                         @RequestParam(value = "returnFormat", required = false, defaultValue = "Detect") String returnFormat,
                                         @RequestParam(value = "showStart", required = false, defaultValue = "N") String showStart,
                                         @RequestParam(value = "reflect", required = false, defaultValue = "N") String reflect,
                                         @RequestParam(value = "username", required = false) String username,
                                         @RequestParam(value = "key", required = false) String key) {
        def hStatus = HttpStatus.OK
        def outputMap = [:]
        def configMap = readConfigIntoConfigMap(configFilename)

        if (checkUser(configMap, username, key)) {
            String path = configMap.get(programID + ".path")
            String prg = configMap.get(programID + ".program")
            String para = configMap.get(programID + ".parameters")
            Integer timeout = configMap.get(programID + ".timeout") as Integer

            if (reflect.take(1).toLowerCase() == "y") { // Reflect back the GET parameters
                outputMap.put("programID", programID)
                params.eachWithIndex { item, i ->
                    outputMap.put("params" + (i + 1), item)
                }
            } else if (showStart.take(1).toLowerCase() == "y") { // Show program start values
                outputMap.put("Start", getCmdLine(programID, path, prg, para, params)[0])
            } else { // Start external program with parameters
                if (prg != null) {
                    if (timeout == null)
                        timeout = -1
                    def sout = new StringBuilder()
                    def serr = new StringBuilder()
                    def responseString
                    try {
                        def cmdLine = getCmdLine(programID, path, prg, para, params)
                        def tempFilename = cmdLine[1]
                        def proc = cmdLine[0].execute()
                        proc.consumeProcessOutput(sout, serr)
                        if (timeout > 0)
                            proc.waitForOrKill(timeout * 1000)
                        else
                            proc.waitFor()
                        responseString = processResults(tempFilename, returnFormat, sout.toString().trim(), serr.toString().trim())
                    }
                    catch (IOException e) {
                        responseString = "Process killed " + e
                        serr.append("")
                    }
                    hStatus = (serr.toString().trim() != "") ? HttpStatus.NOT_FOUND : HttpStatus.OK
                    return new ResponseEntity<Object>(responseString, hStatus)
                } else {
                    outputMap.put("Error", programID + " not configured")
                    hStatus = HttpStatus.NOT_FOUND
                }
            }
        } else {
            outputMap.put("Error", "Not authorized")
            hStatus = HttpStatus.FORBIDDEN
        }

        return new ResponseEntity<Object>(outputMap, hStatus)
    }

    static def getCmdLine(String pID, String path, String prg, String para, String[] params) {
        def tempFilename = System.getProperty("java.io.tmpdir").replaceAll("\\\\","/") + UUID.randomUUID().toString()
        def cmdLine = ""
        if (path != null)
            cmdLine += path + File.separator
        cmdLine += prg

        if (para != null) {
            params.eachWithIndex { item, i ->
                para = para.replaceAll("@@params" + (i + 1) + "@@", item)
            }
            para = para.replaceAll("@@tempFilename@@", tempFilename)
            cmdLine += " " + para
        }

        return [cmdLine, tempFilename]
    }

    static def processResults(String tempFilename, String retFormat, String stdout, String stderr) {
        String retVal

        if (stderr != "")
            retVal = stderr
        else { // Convert stdout or tempFile: Get JSON or make JSON from Lines
            def tempFile = new File(tempFilename)
            if (tempFile.exists()) {
                stdout = tempFile.getText()
                tempFile.delete()
            }
            stdout = stdout.trim()
            if (((stdout.take(1) == "{") && (stdout.reverse().take(1) == "}")) ||
                    ((stdout.take(1) == "[") && (stdout.reverse().take(1) == "]"))) // Primitive JSON detection
                retVal = stdout
            else
                retVal = makeJSONFromReturnValues(stdout, retFormat)
        }

        return retVal
    }

    static def makeJSONFromReturnValues(String stdout, String format) {
        def dataMap = [:]
        format = format.toLowerCase().take(1)
        def lines = stdout.split("\n")
        if ((format == "d") && (lines[0].indexOf("=") > 1)) // detect KeyValue pairs
            format = "k"
        if (format == "k" ) { // KeyValue pairs
            stdout.splitEachLine("=") {
                if (it[1] != null)
                    dataMap.put(it[0].trim(), it[1].trim())
            }
        } else if (["c", ","].contains(format)) { // CSV-String with comma
            return CSVStringToJSON(stdout,",")
        } else if (format == ";") { // CSV-String with semicolon
            return CSVStringToJSON(stdout,";")
        } else { // Lines
            stdout.eachLine {line, lineNumber ->
                dataMap.put("value" + (lineNumber+1), line)
            }
        }
        return JsonOutput.toJson(dataMap)
    }

    static def CSVStringToJSON(csvContent, csvSeparator) {
        def csvs = (csvSeparator != "") ? csvSeparator : ","
        def header = []
        def rows = []
        csvContent.eachLine { line, lineIndex ->
            def rowValues = line.split(csvs + "(?=(?:[^\"]*\"[^\"]*\")*[^\"]*\$)")
            if (lineIndex == 0) // header line
                header = rowValues
            else { // data line
                def singleRowMap = [:]
                rowValues.eachWithIndex { fieldValue, idx ->
                    def s = fieldValue.replaceAll('^"', "").replaceAll('"\$', "")
                    singleRowMap.put(header[idx], s)
                }
                rows.add(singleRowMap)
            }
        }
        return JsonOutput.toJson(rows)
    }

    static def readConfigIntoConfigMap(String cfgFilename) {
        def retVal = [:]
        def configFile = new File(cfgFilename)
        if (configFile.exists()) {
            def cfgContent = configFile.getText().trim()
            retVal.clear()
            cfgContent.splitEachLine("=") {
                if (it[1] != null)
                    retVal.put(it[0], it[1])
            }
        }
        return retVal
    }

    static def checkUser(cfgMap, username, key) {
        return ((cfgMap.get("internal.authorizationrequired").toString().toLowerCase() != "true") || ((cfgMap.containsKey(username + ".key") && (cfgMap.get(username + ".key") == key))))
    }

}
