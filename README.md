# netDelegator

netDelegator is a microservice to access command line programs and deliver the output of these programs as REST responses to the caller

Caution
-
* This microservice contains only basic authorization precautions (username, key)
* Do not open it to the outside world (aka Internet)
* Even in a LAN it can be rather dangerous
* It is designed to provide external program execution via REST API on a local machine only

netDelegator Features
-
* Provides a REST API to external programs
* Multiple programs with multiple parameters can be configured
* Key/Value output will be converted automatically to JSON attribute/value pairs
* Simple line output will be converted automatically to JSON attribute/value pairs with "value" plus number as attribute, line content as value  
* JSON response
* Optional CSV output to JSON response conversion
* Execution of additional Groovy scripts with string responses
* Configuration file
* Configuration is loaded on each request (dynamic configuration without restarting the microservice)
* Server port and configuration filename configurable
* Basic authorization via username and key

Command line parameters
-
* -Dserver.port=8087<br>
In this example the microservice listens on port 8087 for REST requests. Changing the port doesn't make the microservice "invisible" (Security through obscurity doesn't work).  
* -Dconfig.file=netDelegator.cfg<br>
The filename is the filename of the cfg file to configure netDelegator.  Default is netDelegator.cfg in the same directory as the JAR file.

CFG file
-
Per program id there are following keys available: path, program, parameters and timeout:

| Key | Description |
|-----|-------------|
|programID.path|Directory of the program| 
|programID.program|Exec name or batch program|
|programID.parameters|Parameters for program: @@params1@@ @@params2@@... @@tempFilename@@ will be generated automatically and can be used to get the response values if this temp file exists (filled by the external program)|
|programID.timeout|max. runtime of program (in seconds), -1 or 0, no timeout|

Internal parameters:

|Key|Value|Description |
|---|-----|------------|
|internal.showconfig|true or false|Show config at /config endpoint or not|
|internal.allowedScripts|true or false or list of scripts|List of allowed Groovy scripts for /exec or /exectojson endpoint, true allow all scripts, false allow none|
|internal.authorizationrequired|true or false|Authorization via username.key

Example:

|netDelegator.cfg|
|----------------|
|seleniumrunner.path=<br>seleniumrunner.program=seleniumrunner.cmd<br>seleniumrunner.parameters=@@params1@@<br>seleniumrunner.timeout=45<br><br>batch1.path=C:\temp<br>batch1.program=batch1.cmd<br>batch1.parameters=@@params1@@ @@tempFilename@@<br>batch1.timeout=45<br><br>internal.showconfig=true<br>internal.allowedScripts=true<br>internal.authorizationrequired=true<br><br>myusername.key=sifdvbt785678vb4h78345h6hb<br>nextuser.key=789fgsfgsfgw897|

API
-
|Endpoint|Parameters|Description|
|--------|----------|-----------|
| /| | Show config (if enabled in the cfg file)|
| /config| | Show config (if enabled in the cfg file)|
| /exec | script=scriptname&params=parameter<br>optional: &username, &key| Execute groovy script in the same directory as netDelegators JAR file (multiple params possible), response is string|
| /exectojson | script=scriptname&params=parameter<br>optional: &username, &key | Execute groovy script in the same directory as netDelegators JAR file (multiple params possible), response is JSON|
| /start  | programID=programmname&params=parameter<br>optional: &returnFormat, &showStart, &reflect, &username, &key| Execute external program (with multiple params) that is configured in the cfg file<br>|

Usage examples
-
|Example|Description|
|-------|-----------|
|GET http://localhost:8087/config|Show configuration (content of cfg file)|
|GET http://localhost:8087/start?programID=batch1&params=par1&reflect=Y|Dont start batch1, but show request parameters|
|GET http://localhost:8087/start?programID=batch1&params=par1&showStart=Y|Dont start batch1, but show the start statement as configured|
|GET http://localhost:8087/start?programID=seleniumrunner|Start seleniumrunner and convert the output to JSON response|
|GET http://localhost:8087/start?programID=batch1&returnFormat=d|Start batch1 and detect output format (JSON, key/value, simple lines) |
|GET http://localhost:8087/start?programID=batch1&returnFormat=k|Start batch1 and expect output to be key=value lines|
|GET http://localhost:8087/start?programID=batch1&returnFormat=CSV|Start batch1 and expect a CSV return with comma as CSV separator|
|GET http://localhost:8087/start?programID=batch1&params=P1&returnFormat=,|Start batch1 with parameter P1 and expect a CSV return with comma as CSV separator|
|GET http://localhost:8087/start?programID=batch1&params=P1&returnFormat=,&username=myusername&key=sifdvbt785678vb4h78345h6hb|Start batch1 with parameter P1 and expect a CSV return with comma as CSV separator|
|GET http://localhost:8087/start?programID=batch1&params=P1&returnFormat=;|Start batch1 with parameter P1 and expect a CSV return with semicolon as CSV separator|
|GET http://localhost:8087/exec?script=tester&params=P1|Start Groovy script tester with parameter P1 and response as string|
|GET http://localhost:8087/exectojson?script=testJSON&params=P1|Start Groovy script testJSON with parameter P1 and response as JSON|
|GET http://localhost:8087/exectojson?script=testJSON&params=P1&username=nextuser&key=789fgsfgsfgw897|Start Groovy script testJSON with parameter P1 and response as JSON|

License
-
This project is licensed under the MIT License - see the [LICENSE](LICENSE) file for details
